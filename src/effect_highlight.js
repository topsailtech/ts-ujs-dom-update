require("./effect_highlight.css")

function effect_highlight(dom_el){
  dom_el.classList.add("effect_highlight")
  setTimeout(
    ()=>{ dom_el.classList.remove("effect_highlight") },
    2000
  )
}

export default effect_highlight