//
// place <ts-dom-update-form></ts-dom-update-form> anywhere inside your UJS remote form,
//   and the ajax response will be used to update the local DOM.
//
// optional attributes:
//   append
//   prepend
//   before
//   after
//   update
//   replace
//   error_replace (defaults to replace)
//
import { default as updateDomFromFragmentString } from './update_dom_from_fragment_string.js'

class TsDomUpdateForm extends HTMLElement {
  connectedCallback(){
    this.form = this.closest("form")
    if (this.form){
      this.form.addEventListener("ajax:success", ajaxSuccess.bind(this))
      this.form.addEventListener("ajax:error", ajaxError.bind(this))
    }
  }
}

function ajaxSuccess(event){
  event.stopPropagation() // don't let other handlers kick in

  updateDomFromFragmentString(
    event.detail[2].responseText,
    {
      append:  this.getAttribute("append"),
      prepend: this.getAttribute("prepend"),
      before: this.getAttribute("before"),
      after: this.getAttribute("after"),
      update:  this.getAttribute("update"),
      replace: this.getAttribute("replace")
    }
  )
}

function ajaxError(event){
  event.stopPropagation() // don't let other handlers kick in

  updateDomFromFragmentString(
    event.detail[2].responseText,
    { replace: this.getAttribute("error_replace") || this.getAttribute("replace") }
  )
}

customElements.define('ts-dom-update-form', TsDomUpdateForm)
