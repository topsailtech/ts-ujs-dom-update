# ts-ujs-dom-update

A Custom Element to catch rails ujs responses. 
Remote UJS requests performed form a link `<a>` will replace or update local DOM element(s).

## Installation

```
yarn add https://bitbucket.org/topsailtech/ts-ujs-dom-update.git
```
In your pack (as javascript *and* a css resource) ```require('ts-ujs-dom-update')```.

## Usage

To replace exsiting DOM elements that have the same DOM *id* as the returned elements:
```html
<a href="some_url_for_content" is="ts-dom-update-link">Action</a>
```

To append new elements to an existing DOM element:
```html
<a href="some_url_for_content" is="ts-dom-update-link" append="some_item_list">Action</a>
```

To prepend new elements to an existing DOM element:
```html
<a href="some_url_for_content" is="ts-dom-update-link" prepend="some_item_list">Action</a>
```
To remove an element in the existing DOM, the response should include a header *ts-deleted*, with the DOM ID as the value.

When a link <a> is clicked and a sucessful UJS request comes back, local DOM elements will get replaced/appended/prepended with the UJS reply.
