//
//  This file is also used by other web components (ts-modal-ujs-transaction)
//
export default updateDomFromFragmentString

import { default as highlight } from './effect_highlight.js'
import { append_html, prepend_html, replace_html, update_html, html_after, html_before } from 'wc-util'

function updateDomFromFragmentString(new_dom_fragment, {append=null, prepend=null, before=null, after=null, update=null, replace=null}){
  if (new_dom_fragment){
    let target_el_id = append || prepend || after || before || update || replace,
        target_el = document.getElementById(target_el_id)

    if (target_el) { // explicit target element ID given
      if (append) {
        for (let new_dom_el of elementsFromString(new_dom_fragment)){
          append_html(target_el, new_dom_el)
          highlight(new_dom_el)
        }
      } else if (prepend) {
        for (let new_dom_el of elementsFromString(new_dom_fragment).reverse()){
          prepend_html(target_el, new_dom_el)
          highlight(new_dom_el)
        }
      } else if (before) {
        for (let new_dom_el of elementsFromString(new_dom_fragment).reverse()){
          html_before(target_el, new_dom_el)
          highlight(new_dom_el)
        }
      } else if (after) {
        for (let new_dom_el of elementsFromString(new_dom_fragment)){
          html_after(target_el, new_dom_el)
          highlight(new_dom_el)
        }
      } else if (update) {
        update_html(target_el,  new_dom_fragment)
        highlight(target_el)
      }
      else if (replace) {
        replace_html(target_el, new_dom_fragment)
        let new_dom_el = document.getElementById(target_el_id)
        new_dom_el && highlight(new_dom_el)
      }
    } else if (target_el_id) {
      console.log("Can't find target element with ID", target_el_id)
    } else { // no explicit target element given
      for (let root_node of elementsFromString(new_dom_fragment)){
        replaceDomFor(root_node)
      }
    }
  }
}

// returns a HTMLCollection
function elementsFromString(fragment_string){
  let root = document.createElement("template") // using "template" so that TDs etc. will work too
  root.innerHTML = fragment_string
  return Array.from(root.content.children) // Return Array instead of HTMLCollection to make it not "live"
}

function replaceDomFor(new_dom_el){
  let target_el = document.getElementById(new_dom_el.id)
  if (target_el) {
    replace_html(target_el, new_dom_el)
    highlight(new_dom_el)
  } else if (new_dom_el.nodeName == "SCRIPT") {
    eval(new_dom_el.text)
  } else {
    console.log("Don't know where to put", new_dom_el)
  }
}
