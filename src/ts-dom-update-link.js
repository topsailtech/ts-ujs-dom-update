import { default as updateDomFromFragmentString } from './update_dom_from_fragment_string.js'

class TsDomUpdateLink extends HTMLAnchorElement {
  connectedCallback(){
    this.addEventListener("ajax:success", ajaxSuccess.bind(this))
  }
}

function ajaxSuccess(event){
  event.stopPropagation() // don't let other handlers kick in

  let xhr = event.detail[2]
  if (xhr.getResponseHeader("ts-deleted")) {
    let delete_el = document.getElementById(xhr.getResponseHeader("ts-deleted"))
    delete_el && delete_el.remove()
  }

  if (xhr.responseText.trim() != "") {
    updateDomFromFragmentString(
      xhr.responseText,
      {
        append:  this.getAttribute("append"),
        prepend: this.getAttribute("prepend"),
        after:   this.getAttribute("after"),
        before:  this.getAttribute("before"),
        update:  this.getAttribute("update"),
        replace: this.getAttribute("replace")
      }
    )
  }
}

customElements.define('ts-dom-update-link', TsDomUpdateLink, { extends: "a" })
